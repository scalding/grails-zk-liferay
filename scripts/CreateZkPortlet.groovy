target(main: "Creates ZK Liferay portlet configuration file") {
    def displayName = grailsConsole.userInput("Display name:")
    def frontPage = grailsConsole.userInput("Front page (.zul):")
    def description = grailsConsole.userInput("Description:")

    name = displayName.replaceAll("\\W", {'-'}).toLowerCase()

    //TODO: find a better way of doing this...
    fileNameParts = []
    displayName.split(' ').each { part ->
        fileNameParts << part.replaceAll("\\W", {''}).capitalize()
    }
    fileName = fileNameParts.join('')

    def configFile = "${basedir}/grails-app/conf/Liferay/${fileName}PortletConfig.groovy"
    ant.copy(
            file:"${zkLiferayPluginDir}/src/samples/_PortletConfig.groovy",
            tofile: configFile,
            overwrite: true
    )
    ["displayName": displayName, "name": name, "description": description, "frontPage": frontPage].each { key, value ->
        ant.replace(
                file:  configFile,
                token: "@${key}@",
                value: value
        )
    }
}

setDefaultTarget(main)
