import groovy.xml.StreamingMarkupBuilder
import org.springframework.core.io.support.PathMatchingResourcePatternResolver
import org.springframework.core.io.Resource

eventCreateWarStart = { warName, stagingDir ->
    ant.copy(todir: "$stagingDir") {
        fileset(dir: "$basedir/grails-app/zul")
    }

    def searchPath = "file:${basedir}/grails-app/conf/Liferay/**/*PortletConfig.groovy"
    Resource[] portletFiles = (new PathMatchingResourcePatternResolver().getResources(searchPath)).toList()

    generatePortletXml(portletFiles)
    generateLiferayDisplayXml(portletFiles)
    generateLiferayPortletXml(portletFiles)
    generateLiferayPluginPackage()

    generateZkXml()
}

/*
eventPackagingEnd = {
    generatePortletXml()
}
 */

def generateZkXml() {
    def zkXml = generatePortletConfigFile("zk.xml")
    def xmlWriter = new StreamingMarkupBuilder()
    def xml = xmlWriter.bind {
        mkp.yieldUnescaped '<?xml version="1.0" encoding="UTF-8"?>'
        mkp.comment 'generated file, I wouldn\'t edit'

        'zk' {
            def libraryProperties = ['org.zkoss.zk.portlet.PageRenderPatch.class': 'org.zkoss.zkplus.liferay.JQueryRenderCachedPatch',
                    'org.zkoss.zkplus.liferary.jQueryPatch': '500']
            libraryProperties.each { k, v ->
                'library-property' {
                    'name'(k)
                    'value'(v)
                }
            }
        }
    }
    zkXml.write(xml.toString())
}

def generatePortletXml(portletFiles) {
    def portletXml = generatePortletConfigFile("portlet.xml")
    def xmlWriter = new StreamingMarkupBuilder()

    def portletVersion = '2.0'
    def underscoredVersion = "2_0"
    def xml = xmlWriter.bind {
        mkp.yieldUnescaped '<?xml version="1.0" encoding="UTF-8"?>'
        mkp.comment 'generated file, I wouldn\'t edit'
        'portlet-app' {
            portletFiles.each { portletFile ->
                portletConfig = new ConfigSlurper().parse(portletFile.getURL())
                def className =
                    'portlet' {
                        'description'(portletConfig.description ?: 'description')
                        'portlet-name'(portletConfig.name ?: 'portlet-name')
                        'display-name'(portletConfig.displayName ?: 'display-name')
                        'portlet-class'('org.scaldingspoon.LiferayDHtmlLayoutPortlet')
                        'expiration-cache'(0)
                        'supports' {
                            'mime-type'('text/html')
                            'portlet-mode'('view')
                        }
                        'portlet-info' {
                            'title'(portletConfig.displayName ?: 'title')
                            'short-title'(portletConfig.shortTitle ?: (portletConfig.displayName ?: 'short title'))
                            'keyworks'('zk')
                        }
                        'portlet-preferences' {
                            'preference' {
                                'name'('zk_page')
                                'value'(portletConfig.frontPage ?: '/index.zul')
                            }
                        }

                        ['power-user', 'user', 'administrator'].each {role ->
                            'security-role-ref' {
                                'role-name'(role)
                            }
                        }
                    }
            }
        }

    }
    portletXml.write(xml.toString())
}

def generateLiferayDisplayXml(portletFiles) {
    def displayXml = generatePortletConfigFile('liferay-display.xml')
    def xmlWriter = new StreamingMarkupBuilder()

    // there's probably a groovier way to do this, but I don't care.. I've thought about it too long
    _combineMaps = { map1, map2 ->
        def _tmap = map1.clone()
        map2.each {k, v ->
            if (!_tmap.containsKey(k)) {
                _tmap[k] = v
            } else {
                if (v instanceof Map && _tmap[k] instanceof Map) {
                    _tmap[k] = _combineMaps(_tmap[k], v)
                } else {
                    _tmap[k] = [_tmap[k], v]
                }
            }
        }
        return _tmap
    }
    def _map = [:]
    portletFiles.each { portletFile ->
        def _tmap = [:]
        portletConfig = new ConfigSlurper().parse(portletFile.getURL())
        if (portletConfig.category instanceof String) {
            _tmap[portletConfig.category] = portletConfig.name
        } else {
            portletConfig.category.reverse().each { key ->
                if (_tmap.isEmpty()) {
                    _tmap[key] = portletConfig.name
                } else {
                    _tmap = [(key): _tmap]
                }
            }
        }
        _map = _combineMaps(_map, _tmap)
    }

    _helpme = { xml, args ->
       if (args instanceof String) {
           xml."portlet"(id: args)
       }
       else if (args instanceof Map) {
           args.each { key, value ->
               xml."category"(name: key) {
                   _helpme(xml, value)
               }
           }
       } else if (args instanceof List) {
           args.each {
               _helpme(xml, it)
           }
       }
    }

    def xml = xmlWriter.bind {
        mkp.xmlDeclaration()
        mkp.comment 'generated file, I wouldn\'t edit'
        'display' {
            _helpme(xml, _map)
        }
    }
    displayXml.write(xml.toString())
}

def generateLiferayPortletXml(portletFiles) {
    def displayXml = generatePortletConfigFile('liferay-portlet.xml')
    def xmlWriter = new StreamingMarkupBuilder()
    def xml = xmlWriter.bind {
        mkp.yieldUnescaped '<?xml version="1.0" encoding="UTF-8"?>'
        mkp.comment 'generated file, I wouldn\'t edit'
        'liferay-portlet-app' {
            portletFiles.each {portletFile ->
                portletConfig = new ConfigSlurper().parse(portletFile.getURL())
                'portlet' {
                    'portlet-name'(portletConfig.name ?: 'portlet-name')
                    'header-portlet-javascript'('/zkau/web/js/zk.wpd')
                }
            }
            ['user': 'User', 'power-user': 'Power User', 'administrator': 'Administrator'].each { k, v ->
                'role-mapper' {
                    'role-name'(k)
                    'role-link'(v)
                }
            }
        }
    }

    displayXml.write(xml.toString())
}

def generateLiferayPluginPackage() {
    def displayXml = generatePortletConfigFile('liferay-plugin-package.xml')
    def xmlWriter = new StreamingMarkupBuilder()

    def portletConfig = new ConfigSlurper().parse(new URL("file:${basedir}/grails-app/conf/Liferay/LiferayPluginConfig.groovy"))

    def xml = xmlWriter.bind {
        mkp.yieldUnescaped '<?xml version="1.0" encoding="UTF-8"?>'
        mkp.comment 'generated file, I wouldn\'t edit'

        'plugin-package' {
            'name'(portletConfig.name ?: 'portlet-name')
            'module-id'(portletConfig.moduleId ?: 'module-id')
            'types' {
                'type'('portlet')
            }
            if (portletConfig.tags) {
                'tags' {
                    portletConfig.tags.each { value ->
                        'tag'(value)
                    }
                }
            }
            'short-description'(portletConfig.description ?: 'short description')
            'licenses' {
                'license'('BSD', 'osi-approved': 'true')
            }
            'liferay-versions' {
                portletConfig.versions.each { version ->
                    'liferay-version'(version)
                }
            }
            'deploymet-settings' {
                'setting'('name': '', value: '')
            }
        }
    }
    displayXml.write(xml.toString())
}

def generatePortletConfigFile(String filename) {
    def fileToMake = "${stagingDir}/WEB-INF/${filename}"
    def xml = new File(fileToMake)
    if (xml.exists()) {
        xml.delete()
    }
    return xml
}
