package org.scaldingspoon

import org.zkoss.zk.ui.Session
import com.liferay.portal.util.PortalUtil
import org.zkoss.zk.ui.http.DHtmlLayoutPortlet

class LiferayDHtmlLayoutPortlet extends DHtmlLayoutPortlet {
    @Override
    protected boolean process(Session sess, javax.portlet.RenderRequest request, javax.portlet.RenderResponse response, String path, boolean bRichlet) {
        def user = PortalUtil.getUser(request)
        sess.setAttribute("USER_SCREEN_NAME", user.getScreenName())
        return super.process(sess, request, response, path, bRichlet)    //To change body of overridden methods use File | Settings | File Templates.
    }
}
